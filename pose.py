import os

from PyQt5.QtGui import QPixmap

import file_tools.auto_param_config as apc # TODO change this

class Silhouette:
    """Gère l'image principale d'une pose"""

    def __init__(self, filename):
        self.filename = filename
        self.pixmap = QPixmap(filename)


class Pose:
    def __init__(self, ressource_directory, pose_name, warnings_enabled=True, except_if_inexistant=False,
                 force_conf_generation=False):
        self.pose_name = pose_name
        self.valide = False
        self.proba = 1
        self.difficulty=0
        if pose_name is None:
            self._init_empty(ressource_directory)
        else:
            self._load_pose( ressource_directory, pose_name, warnings_enabled=warnings_enabled,
                            except_if_inexistant=except_if_inexistant, force_conf_generation=force_conf_generation)

    def _load_pose(self, ressource_directory, pose_name, warnings_enabled=True,
                   except_if_inexistant=False, force_conf_generation=False):
        """Essai de charger l'image du fond
        précondition: les atttribus silhouette_widget et title_widget du Pose_Manager sont initialisés"""
        self.pose_name = pose_name
        silhouette_filename = ""
        # title_filename = ""
        # self.title = None
        self.sil = None
        # lire le fichier de conf
        conf_path = os.path.join(ressource_directory, "pose_configs", pose_name + ".conf")
        default_conf = {"pose_name": pose_name,
                        "silhouette_filename": os.path.join("silhouette_storage", pose_name + ".png"),
                        "title_filename": os.path.join("title_storage", pose_name + "_title.png"),
                        "proba": 1.0,
                        "difficulty": 0}
        conf_data = apc.text_conf_param(default_conf, conf_path, warnings_enabled=warnings_enabled, verbose=False,
                                        rewrite_conf_when_fail=force_conf_generation)

        if conf_data is None or conf_data == {}:
            # Erreur de lecture de la config
            if warnings_enabled:
                print("Warning! fichier de conf de la pose inexistant (", pose_name + ".conf", ")")
            if except_if_inexistant:
                # retourner une erreur
                raise ValueError("Fichier de conf de pose inexistant")
        else:
            silhouette_filename = os.path.join(ressource_directory, conf_data["silhouette_filename"])
            # title_filename = ressource_directory + conf_data["title_filename"]
            self.pose_name = conf_data["pose_name"]
            self.proba = conf_data["proba"]
            self.difficulty = conf_data["difficulty"]

        if os.access(silhouette_filename, os.R_OK):
            self.sil = Silhouette(silhouette_filename)
            self.valide = True
        # self.title = Title(title_filename, pose_name)
        # self.title.adjust_size(pose_manager.title_widget.width(), pose_manager.title_widget.height())

    def _init_empty(self, ressource_directory, warnings_enabled=True, except_if_inexistant=False):
        #self.title = Title(ressource_directory + "vide.png", "")
        self.sil = Silhouette(ressource_directory + "vide.png")
        self.valide = True
        self.pose_name = ""
        self.proba = 0
        self.difficulty = 0

    def get_silhouette(self):
        return self.sil

    @staticmethod
    def load_all_poses(ressource_directory, load_all_images=False, verbose=False,
                       filter_valid=True, dev_mode=False):
        """Essai de charger toutes les poses ayant un fichier de conf dans le sous-dossier 'pose_configs' du dossier
        ressource fourni
        PARAMS: - ressource_directory : path-like = le chemin du dossier de ressource du jeu
                -[load_all_images]: boolean = [False] si True les images (.png/.jpg) présentent dans le dossiers
                'silhouette_storage' mais ne comportant pas de ficher de conf associées seront aussi chargées et une
                configuration automatique leur sera créé.
                -[verbose]: boolean = [False] si True, la liste des fonds trouvés sera affichée ainsi que leur validité
                (contient tous les fonds valides où non)
                -[filter_valid]: boolean = [True] si True seul les fonds correctment chargés seront revoyés.
        Postcondition : le dictionnaire pose_dict des fonds est chargé"""
        if dev_mode:
            print("   Loading Poses...")
        POSE_DICT = {}
        # lister tous les nom de poses (c'est a dire tous les fichier d'extension '.conf' du dossier
        # background_storage en coupant l'extension en question)
        name_list_c = [path[:-5] for path in os.listdir(os.path.join(ressource_directory, "pose_configs")) if
                       path.find(".conf") > -1]
        # Tenter de charger tous les poses listées
        for pose_name in name_list_c:
            POSE_DICT[pose_name] = Pose(ressource_directory, pose_name, force_conf_generation=False)
            if verbose:
                print("Loading pose", pose_name, "from config; succès :", POSE_DICT[pose_name].valide)

        # ajouter les nom des fichers .png si l'option est activée, en évitant les doublons
        if load_all_images:
            name_list_i = [path[:-4] for path in os.listdir(os.path.join(ressource_directory, "silhouette_storage")) if
                           path.find(".png") > -1 and not (path[:-4] in name_list_c)]
            # Tenter de charger tous les fond listées en générant toujours les config
            for pose_name in name_list_i:
                POSE_DICT[pose_name] = Pose(ressource_directory, pose_name, except_if_inexistant=False,
                                                force_conf_generation=True)
                if verbose:
                    print("Loading pose", pose_name, "from image (auto config generated); succès :", POSE_DICT[pose_name].valide)
        # ajout de l'image vide
        POSE_DICT[""] = Pose(ressource_directory, None, except_if_inexistant=True,
                                 force_conf_generation=False)

        if filter_valid:
            if verbose:
                print("Filtrage des poses invalides")
            r_list = []
            for pose_name in POSE_DICT.keys():
                if not POSE_DICT[pose_name].valide:
                    r_list.append(pose_name)
                    if verbose:
                        print("WARNING : Pose invalite filtrée '{}'".format(pose_name))
            for r_pose_name in r_list:
                POSE_DICT.pop(r_pose_name, None)

        if dev_mode:
            print("OK")
        MAX_DIFFICULTY_LEVEL = max([p.difficulty for p in POSE_DICT.values()])

        return POSE_DICT, MAX_DIFFICULTY_LEVEL


def get_random_nonnull_silhouette(pose_dict):
    for k in pose_dict:
        if pose_dict[k].get_silhouette().pixmap.height() > 0:
            return pose_dict[k].get_silhouette()
    return None