import threading
import time
from enum import Enum

import os
from PyQt5.QtCore import QObject, pyqtSignal

from pose import Pose
from TwistCamConf import TCConfig
from Sequence_Generator import Generate_Sequence

class GameState(Enum):
    IDLE = 0  # application dans son état initial, attend une commande pour agir # TODO a changer
    PREVIEW = 1  # mode préview, affiché en condition réelle (coté TVn7)
    PREVIEW_HIDE = 2  # mode préview affiché coté net7
    WAIT_PLAY = 3  # partie prête, attend seulement la top départ.
    STARTING_PLAYING = 4 # Partie en cours de démmarage, pendant l'annimation
    PLAYING = 5  # partie en cours d'execution
    PLAYING_TOGGLE = 6 # delai entre deux tour
    WAIT_RELOAD = 7  # partie terminé attend que TVn7 change d'écran pour revenir en HIDLE
    ENDING = 8  # Toute l'application doit s'arreter.


class Player(QObject):

    def __init__(self, index, color, gc):
        super().__init__()
        self.index = index
        self.color = color
        self.lifes = gc.config.nbr_lifes
        self.current_pose = None
        self.valide = None
        gc.pose_changed_signal.connect(self._change_poses)

    def _change_poses(self, new_poses):

        self.current_pose = (new_poses[self.index] if self.lifes != 0 else "")
        self.valide = self.lifes <= 0
        print("Validation of {} reset to {}".format(self.index, self.valide))


class GameCore(QObject, threading.Thread):
    # Signaux
    start_game_signal = pyqtSignal()
    game_started_signal = pyqtSignal()
    end_round_signal = pyqtSignal()
    win_signal = pyqtSignal(int)
    end_game_signal = pyqtSignal()
    reload_signal = pyqtSignal()

    time_changed_signal = pyqtSignal(str)
    player_validation_signal = pyqtSignal(int)
    toggle_tick_signal = pyqtSignal()
    pose_changed_signal = pyqtSignal(tuple)
    game_state_changed = pyqtSignal(GameState)
    life_lost_signal = pyqtSignal(int, int)

    size_config_changed_signal = pyqtSignal(TCConfig)

    exit_game_signal = pyqtSignal()

    POSE_DICT = {}
    MAX_DIFFICULTY_LEVEL = 0

    def __init__(self, config:TCConfig):
        QObject.__init__(self)
        threading.Thread.__init__(self, target=self.core_loop_thread)

        # Parameter of the game
        self.config = config
        config.edit_signal = self.size_config_changed_signal
        self.game_state = GameState.WAIT_RELOAD
        self.players = None
        self.sequence = None
        self.time_value = 0
        self.toggle_time = None
        self.sequence_index = None
        self.timer_mode = TimerMode.DISABLED

    @classmethod
    def init_pose_dict(cls, config):
        """QApplication must be init before this methode was called"""
        if cls.POSE_DICT == {} and cls.MAX_DIFFICULTY_LEVEL == 0:
            cls.POSE_DICT, cls.MAX_DIFFICULTY_LEVEL \
                = Pose.load_all_poses(config.ressources_directory, load_all_images=True, #TODO config?
                                      verbose=config.verbose_level > 1, filter_valid=True, #TODO config?
                                      dev_mode=config.verbose_level == 3)

    def core_loop_thread(self):
        self.size_config_changed_signal.emit(self.config)
        self.game_state_changed.emit(self.game_state)
        while not (self.game_state == GameState.ENDING):
            while self.game_state == GameState.PLAYING or \
                  self.game_state == GameState.PLAYING_TOGGLE:
                if self.game_state == GameState.PLAYING:
                    if self.time_value < 0:
                        if self.config.timer_mode == TimerMode.EACH_POSE:
                            if self.config.verbose_level > 0:
                                print("    |GameCore, time-up : remaining people %s lose 1 life." %
                                      [k for k in range(self.config.nbr_player) if not self.players[k].valide])
                            self.end_round()
                        elif self.config.timer_mode == TimerMode.FULL_GAME:
                            if self.config.verbose_level > 0:
                                print("    |GameCore, time-up : everyone lose.")
                            self.end_game()
                if self.game_state == GameState.PLAYING_TOGGLE:
                    if self.toggle_time < 0:
                        # Fin de la pause entre deux tours
                        self.toggle_next_pose(1)
                    else:
                        self.toggle_time -= self.config.timer_tick_duration
                    print("tick", self.toggle_time)

                if not self.timer_mode == TimerMode.DISABLED:
                    self.set_time(self.time_value - self.config.timer_tick_duration,
                                  verbose=self.config.verbose_level > 0)
                self.toggle_tick_signal.emit()
                time.sleep(self.config.timer_tick_duration)
            time.sleep(0.5)
        print("#Core thread exited.")

    def play_slot(self):
        if self.game_state == GameState.WAIT_PLAY:
            self.game_state = GameState.STARTING_PLAYING
            self.start_game_signal.emit()
            #env["tc_win"].intro.play_intro_signal.emit()  # TODO disable by config
            if self.config.verbose_level > 0:
                print("   |GameCore, play command: OK, Starting Game.")
            return True
        else:
            if self.config.verbose_level > 0:
                print("   |GameCore, play command: Not ready, starting aborted.")
            return False

    def count_not_valide(self):
        return [p.valide for p in self.players].count(False)

    def count_alive(self):
        return [p.lifes > 0 for p in self.players].count(True)

    def set_game_state(self, new_game_state: GameState):
        # TODO check if previous state is compatible
        self.game_state = new_game_state
        if self.config.verbose_level > 1:
            print("Game State changed :", new_game_state)
        if new_game_state == GameState.PLAYING_TOGGLE:
            self.toggle_time = self.config.toggle_duration

        if new_game_state == GameState.PLAYING or new_game_state == GameState.PLAYING_TOGGLE or new_game_state == GameState.STARTING_PLAYING:
            if not self.config.locked:
                self.config.config_lock()
        else:
            if self.config.locked:
                self.config.config_unlock()

        self.game_state_changed.emit(self.game_state)

    def intro_end_slot(self):
        self.set_game_state(GameState.PLAYING)

    def validate_player_pose_slot(self, player_id):
        if (self.game_state == GameState.PREVIEW or self.game_state == GameState.PLAYING):
            if not self.players[player_id].valide:
                if self.config.verbose_level > 0:
                    print("    |Validation du joueur {}".format(player_id))
                self.players[player_id].valide = True
                self.player_validation_signal.emit(player_id)

                if self.count_not_valide() <= 1:
                    self.end_round()
            else:
                print("Player {} is already validated!".format(player_id))
        else:
            print("Validation refused, game state ({}) invalid for this action." .format(self.game_state))

    def end_round(self):
        self.timer_mode = TimerMode.DISABLED
        self.set_game_state(GameState.PLAYING_TOGGLE)
        for k, p in enumerate(self.players):
            if not p.valide:
                p.lifes -= 1
                self.life_lost_signal.emit(k, p.lifes)
                #red_switching_thread = threading.Thread(target=color_switching, # TODO move to pose widget
                #                                        args=(env["tc_win"], k, "#D00000", self.config.animation_delay))
                #red_switching_thread.start()

    def set_time(self, new_time, verbose=False):
        changed = not round(new_time) == round(self.time_value)
        self.time_value = new_time
        if changed:
            self.time_changed_signal.emit(str(round(self.time_value)))
            if verbose:
                print("    |GameCore, timer changed : {}".format(round(self.time_value)))

    def toggle_next_pose(self, n):
        self.sequence_index += n
        if self.sequence_index < len(self.sequence):
            alive_count = self.count_alive()
            if alive_count > 1:
                new_poses = tuple(pose_name if self.players[k].lifes > 0 else None
                                               for k, pose_name in enumerate(self.sequence[self.sequence_index][0]))
                self.pose_changed_signal.emit(new_poses)
                # --- BUGFIX SHLAG ---
                for p in self.players:
                    p._change_poses(new_poses)
                # --------------------
                # reset timer if necessary
                if self.config.timer_mode == TimerMode.EACH_POSE:
                    self.set_time(self.sequence[self.sequence_index][1])
                print("    |GameCore, switch next pose", self.sequence[self.sequence_index][0])
                # Restart timer
                self.timer_mode = self.config.timer_mode
                self.set_game_state(GameState.PLAYING)
                self.toggle_time = None
            else:
                if alive_count == 0:
                    self.end_game(winner=None) # TODO move end_game call to main process? Bof
                else:
                    self.end_game(winner=[p.lifes > 0 for p in self.players].index(True))
        else:
            self.end_game(winner=None)

    def end_game(self, winner=None):
        if self.game_state == GameState.WAIT_PLAY or self.game_state == GameState.PLAYING or self.game_state == GameState.PLAYING_TOGGLE: # TODO pourquoi WAIT_PLAY?
            self.set_game_state(GameState.WAIT_RELOAD)
            self.timer_mode = TimerMode.DISABLED
            self.time_value = 0
            self.sequence_index = None
            self.sequence = None

            # Display ending feedback
            self.win_signal.emit(None)
            threading.Thread(target=self.execute_win_animation, args=(winner,)).start()

            print("    |GameCore, Game terminating!")
            return True
        else:
            return False

    def execute_win_animation(self, winner_id):
        if winner_id is not None:
            for p in self.config.wi_seq[1]:
                self.pose_changed_signal.emit(tuple(p if k == winner_id else "" for k in range(self.config.nbr_player)))
                time.sleep(self.config.wi_seq[0])
                # TODO interrompue trop tôt
        else:
            time.sleep(1)
        print("    |GameCore, Game ended!")
        self.end_game_signal.emit()

    def reset_game_slot(self):
        # TODO faire apparaitre les joueur au fur et a mesure
        if self.config.verbose_level > 0:
            print(" |Game Core: reloading game signal.")

        if self.game_state == GameState.IDLE or self.game_state == GameState.WAIT_RELOAD: # TODO why IDLE
            # try generate a sequence if it doesn't exist yet
            if self.check_seq_exist():
                self.sequence_index = 0
                self.timer_mode = self.config.timer_mode
                self.set_time(self.config.start_pose_delay + self.sequence[0][1])
                self.players = [Player(k, self.config.player_colors[k], self) for k in range(self.config.nbr_player)]
                self.set_game_state(GameState.WAIT_PLAY)
                self.reload_signal.emit()
                self.pose_changed_signal.emit(self.sequence[0][0])
                return True
        return False

    def check_seq_exist(self):
        # Check if sequence already exist
        if self.sequence is None or len(self.sequence) == 0:
            # Select generation mode, TODO add other modes
            if self.config.generator_mode == "random":
                # mode random donc on génère une sequence a la volé
                self.sequence = Generate_Sequence(self.config.generator_mode,
                                                  self.config.nbr_poses,
                                                  self.POSE_DICT,
                                                  self.config.pose_delay,
                                                  self.config.nbr_player,
                                                  start_pose=self.config.start_pose,
                                                  max_difficulty_level=self.MAX_DIFFICULTY_LEVEL,
                                                  delay_reduction_factor=self.config.reduction_factor)
        if not (self.sequence== None or len(self.sequence) == 0):
            print("    |Séquence OK.")
            return True
        else:
            print("Impossible de démarrer le jeu, aucune sequence n'est définie.")
            return False

    def exit_slot(self):
        self.set_game_state(GameState.ENDING)
        self.exit_game_signal.emit()


class TimerMode(Enum):
    DISABLED = 0  # pas de timer
    FULL_GAME = 1  # One timer for the full game, if it reach 0 every one lose
    EACH_POSE = 2  # The timer is reset for every poses.


if __name__ == '__main__':
    print("Ce module ne fait rien seul, veuillez utiliser Main_TwisterCam.py ")

