import sys
import threading
import time

import nc_control

sys.path.append("./affichage_QT")
from affichage_QT import camWindow, controlWindow
import Game_Core_2 as gc
# import gameUI as gu


if __name__ == '__main__':
    if "help" in sys.argv:
        print("Help not implemented yet!")  # TODO
        sys.exit(0)

    print("Starting TWISTERCAM")

    print("|Importing config file...")
    from TwistCamConf import TCConfig
    config = TCConfig()

    print("|Initiating core thread...")
    game_core = gc.GameCore(config)
    game_core.setDaemon(True)

    print("|Initiating game window...")
    cam_window = camWindow.CamWindow(game_core)

    print("|Initiating control window...")
    control_window = controlWindow.ControlWindow(game_core, cam_window)
    control_window.setDaemon(True)

    #print("|Generating control thread...") #TODO  Est ce qu'on le garde?
    #terminal_thread = threading.Thread(target=gu.control_thread, args=(env, config))
    #terminal_thread.setDaemon(True)

    if config.enable_remote_control:
        print("|Initiating remote TCP control")
        tcp_thread = threading.Thread(target=nc_control.start_nc_receiver, args=(game_core,))
        tcp_thread.setDaemon(True)
    else:
        print("|Remote TCP control is disabled")

    print("|Starting all threads")
    game_core.start()
    control_window.start()
    # terminal_thread.start()
    if config.enable_remote_control:
        tcp_thread.start()

    # Start graphical interface # TODO restart if rebuild
    cam_window.exec_()
    print(" |Graphical interface exited.")

    print("#Graphical thread (main) exited.")
    # Extinction
    time.sleep(2)
    print("                    Game made by Kasonnara, thanks for playing!")
    sys.exit(0)
