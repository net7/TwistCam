import os
import threading

import netifaces
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver

from Game_Core_2 import GameCore

player_allocation = None


class NcReceiver(LineReceiver):
    def __init__(self, game_core: GameCore):
        self.game_core = game_core  # for calling validation
        self.allocation = None  # id of the player affected to the TCP connexion

    def connectionMade(self):
        print("Connection made.")
        global player_allocation
        try:
            for k in range(len(player_allocation), self.game_core.config.nbr_player):
                player_allocation.append(False)
            self.allocation = player_allocation.index(False)
            player_allocation[self.allocation] = True
            self.send_str(("Connexion établie\n" +
                           "Vous controlez le %de joueur (en partant de la gauche)\n" +
                           "Entrez 'h' pour l'aide")
                          % (self.allocation+1,))
            print("Connexion établie, pour manager le joueur %d" % (self.allocation+1,))
        except ValueError:
            print("Too much TCP connexion, not enough players!")
            self.send_str("Too much TCP connexion, not enough players!")
            self.transport.loseConnection()

    def eject(self, n):
        player_allocation[n] = False
        self.send_str("Player %d ejected" % n+1)

    def dataReceived(self, data:str):
        global player_allocation
        #print("Receved data from player %d :%s" %(self.allocation, data))
        if data.startswith(b"p"):
            print("Remote play command")
            if self.game_core.play_slot():
                self.send_str("Game started")
            else:
                self.send_str(
                    "Current game state ({}) is not compatible with 'play' command.".format(self.game_core.game_state))
        elif data.startswith(b"k"):
            if self.game_core.end_game():
                self.send_str("Game stopped")
            else:
                self.send_str(
                    "Current game state ({}) is not compatible with 'break' command.".format(self.game_core.game_state))
        elif data.startswith(b"s"):
            self.send_str("Game state:{}\n{}/{} moderateur connected.".format(self.game_core.game_state,
                                                                              player_allocation.count(True),
                                                                              self.game_core.config.nbr_player))
        elif data.startswith(b"r"):
            if self.game_core.reset_game_slot():
                self.send_str("Game reloaded")
            else:
                self.send_str(
                    "Current game state ({}) is not compatible with 'reset' command.".format(self.game_core.game_state))
        elif data.startswith(b"1"):  #TODO SLaggggg
            self.eject(0)
        elif data.startswith(b"2"):
            self.eject(1)
        elif data.startswith(b"3"):
            self.eject(2)
        elif data.startswith(b"4"):
            self.eject(4)

        elif data.startswith(b"h"):
            self.send_str("Vous modérez le {}e joueur en partant de la gauche\n".format(self.allocation+1)+
                          "Le premier caractère de chaque message envoyé est inspecté \n" +
                          "et interprété s'il correspond à l'une des commandes suivantes:\n" +
                          "p : play \n" +
                          "k : break\n" +
                          "s : status\n" +
                          "r : reload\n" +
                          "h : help \n" +
                          "1|2|3|4: libère la place du modérateur 1, 2, 3 ou 4 en force\n" +
                          "* : Pour tout autre message le joueur {} sera validé.".format(self.allocation))
        else:
            if (self.allocation < self.game_core.config.nbr_player):
                validation_thread = threading.Thread(target=self.game_core.validate_player_pose_slot,
                                                     args=(self.allocation,))
                validation_thread.setDaemon(True)
                validation_thread.start()
                self.send_str("Validation reçue (player {}).".format(self.allocation+1))
            else:
                self.send_str("Validation refusé, cotre joueur associé ({}) n'est pas en jeu (nombre de joueur en jeu :{})".format(self.allocation+1, self.game_core.config.nbr_player))

    def connectionLost(self, reason):
        if self.allocation is not None:
            print("Connection lost with player %d manager. Reason : %s" % (self.allocation + 1, reason))
            global player_allocation
            player_allocation[self.allocation] = False

    def send_str(self, s, end='\n'):
        """Wrapper for having a print equivalent user side."""
        self.transport.write(''.join((s, end)).encode())


class ValiderFactory(Factory):
    def __init__(self, game_core):
        self.game_core = game_core

    def buildProtocol(self, addr):
        return NcReceiver(self.game_core)


def print_own_ip_and_port(port):
    # TODO add color if on linux
    #os.system("ip a | grep 'inet ' | grep 'wlan'")
    interfaces = []
    for netifaces.ifaceName in netifaces.interfaces():
        if netifaces.ifaceName != "lo":
            addresses = [i['addr'] for i in netifaces.ifaddresses(netifaces.ifaceName).setdefault(netifaces.AF_INET, [{'addr': 'No IP address'}])]
            interfaces.append('     - interface {0} : {1}'.format(netifaces.ifaceName, ', '.join(addresses)))
    print(" |Run tcp MODERATION on port {0}, ip:\n{1}".format(port, "\n".join(interfaces)))


def start_nc_receiver(game_core: GameCore, port=1117):
    # Init TCP remote control
    global player_allocation
    player_allocation = [False] * game_core.config.nbr_player
    endpoint = TCP4ServerEndpoint(reactor, port)
    endpoint.listen(ValiderFactory(game_core))
    print_own_ip_and_port(port)
    reactor.run(installSignalHandlers=0)
    print("|TCP Moderation receiver exited.")

