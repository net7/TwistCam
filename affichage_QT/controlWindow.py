import functools
import os
import threading

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtWidgets import QApplication, QWidget, QStackedLayout, QVBoxLayout, QHBoxLayout, QPushButton, QGridLayout, \
    QScrollBar, QDesktopWidget, QLabel, QSlider, QRadioButton, QBoxLayout, QColorDialog, QCheckBox, QFrame

from Game_Core_2 import GameCore, GameState
from TwistCamConf import TCConfig
from camWindow import CamWindow
from keyEventTabWidget import KeyEventTabWidget

# Customisation du style des cadres: (Box, Panel, winPanel, StyledPanel + Plain, Raised, Sunken
FRAME_STYLE = QFrame.Box + QFrame.Raised
FRAME_LINE_WIDTH = (1, 0) # valeur possible des deux elements [0,1,2,3] cf https://doc.qt.io/archives/qt-4.8/qframe.html#details

class ControlWindow(QApplication, threading.Thread):

    # ----- Key reaction -----
    def key_press_event_callback(self, e):
        if e.key() == Qt.Key_Escape:
            self.game_core.exit_slot()
        elif e.key() in self.game_core.config.validate_keys:
            self.game_core.validate_player_pose_slot(self.game_core.config.validate_keys.index(e.key()))
        elif e.key() == 80:  # Todo config key
            self.game_core.play_slot()

    def exec_and_kill(self):
        # Run QApplication
        self.exec_()
        # Then exit the whole program
        if not self.game_core.game_state == GameState.ENDING:
            self.game_core.exit_slot()

    def exit_slot(self):
        super(QApplication, self).exit(0)

    def __init__(self, game_core: GameCore, cam_win: CamWindow):
        QApplication.__init__(self, [])
        threading.Thread.__init__(self, target=self.exec_)

        self.game_core = game_core

        self.reglage_widget = RegalgeWidget(game_core, cam_win)
        self.sequence_widget = SequenceWidget()
        self.drive_widget = DriveWidget(game_core)
        self.help_widget = QWidget()

        self.main_widget = KeyEventTabWidget(self.key_press_event_callback)
        self.main_widget.setWindowTitle("TwistCam Control Room")
        self.main_widget.setWindowIcon(QIcon(os.path.join(game_core.config.ressources_directory, 'TCicon.png')))
        self.main_widget.addTab(self.reglage_widget, "Réglages") # TODO ajouter un QICON
        self.main_widget.addTab(self.sequence_widget, "Séquences") # TODO ajouter un QICO
        self.main_widget.addTab(self.drive_widget, "Contrôle") # TODO ajouter un QICO
        self.main_widget.addTab(self.help_widget, "Aide")  # TODO ajouter un QICO
        self.main_widget.show()

        self.game_core.exit_game_signal.connect(self.exit_slot)


class AbstractConfigLock:
    def setup(self, game_core):
        self.game_core = game_core
        self.game_core.size_config_changed_signal.connect(self.lock_with_conf)

    def lock_with_conf(self, config: TCConfig):
        self.setEnabled(not config.locked)
        #print("lock state :",not config.locked)


class RegalgeWidget(QWidget, AbstractConfigLock):
    def __init__(self, game_core: GameCore, cam_win: CamWindow):
        super(QWidget, self).__init__()
        AbstractConfigLock.setup(self, game_core)

        screen_manage_widget = ScreenManagingWidget(game_core, cam_win)
        game_param_widget = GameParameterWidget(game_core)
        pose_param_widget = PoseParameterWidget(game_core)
        config_file_widget = ConfigFileWidget()

        main_layout = QGridLayout()
        main_layout.addLayout(TitleWidget("Gestion des écrans"), 0, 0, 1, 1)
        main_layout.addWidget(screen_manage_widget, 1, 0, 1, 1)
        main_layout.addLayout(TitleWidget("   Paramètres de jeu"), 0, 1, 1, 1)
        main_layout.addWidget(game_param_widget, 1, 1, 1, 1)
        main_layout.addLayout(TitleWidget("   Paramètres de rendu"), 0, 2, 1, 1)
        main_layout.addWidget(pose_param_widget, 1, 2, 1, 1)
        main_layout.addWidget(config_file_widget, 1, 3, 1, 1)
        self.setLayout(main_layout)


class ScreenManagingWidget(QFrame):
    def __init__(self, game_core:GameCore, cam_win: CamWindow):
        super().__init__()
        self.setFrameStyle(FRAME_STYLE)
        self.setLineWidth(FRAME_LINE_WIDTH[0])
        self.setMidLineWidth(FRAME_LINE_WIDTH[1])


        self.screen_order_ref = game_core.config.screen_order  #/!\ Keeping the reference to the list in the config
        self.screen_selected_slot = cam_win.move_to_screen

        #left_push_button = QPushButton("<<=")
        #right_push_button = QPushButton("=>>")

        self.full_screen_button = QPushButton("Maximiser")
        self.full_screen_button.pressed.connect(cam_win.toggle_fullscreen_slot)
        cam_win.full_screen_changed.connect(self.update_fullscreen_btn)

        update_button = QPushButton("Actualiser\nla liste")
        update_button.pressed.connect(self.update_screens_slot)

        self.screen_widgets = []
        self.push_up_btns = []

        self.main_layout = QGridLayout()
        #self.main_layout.addWidget(left_push_button, 1, 0, 1, 2)
        #self.main_layout.addWidget(right_push_button, 1, 2, 1, 2)
        self.main_layout.addWidget(self.full_screen_button, 2, 0, 1, 4)
        self.main_layout.addWidget(update_button, 3,0,1,4)
        self.setLayout(self.main_layout)

        self.desk_widget = QDesktopWidget()
        self.desk_widget.screenCountChanged.connect(self.update_screens_slot)

        self.update_screens_slot()

    def update_fullscreen_btn(self, fullscreen:bool):
        self.full_screen_button.setText("Minimiser" if fullscreen else "Maximiser")

    def update_screens_slot(self, *args, **kwargs):
        # Check realy available screen number and order
        screen_count = QDesktopWidget().screenCount()
        for k in range(len(self.screen_order_ref), screen_count):
            self.screen_order_ref.append(k)
        reel_screen_order = [k for k in self.screen_order_ref if k < screen_count]

        # Clear layout
        for w in self.screen_widgets + self.push_up_btns:
            self.main_layout.removeWidget(w)
        # Rebuild widgets if necessary
        if not len(self.screen_widgets) == screen_count:
            for w in self.screen_widgets + self.push_up_btns:
                w.deleteLater()
            self.screen_widgets = \
                [ScreenWidget(k, functools.partial(self.screen_selected_slot, k, self.screen_order_ref))
                 for k in range(screen_count)]

        # Reinsert widget in the right order
        self.push_up_btns = []
        for k in reel_screen_order:
            self.main_layout.addWidget(self.screen_widgets[k], 3 * k + 4, 0, 3, 3)
            if k>0:
                btn = QPushButton("<")
                btn.pressed.connect(functools.partial(self.push_up_screen, k))
                self.push_up_btns.append(btn)
                self.main_layout.addWidget(btn, 3 * k + 5, 3, 1, 1)

    def push_up_screen(self, screen_index):
        # Swap indexes and update
        self.screen_order_ref[screen_index], self.screen_order_ref[screen_index-1] = \
            self.screen_order_ref[screen_index-1], self.screen_order_ref[screen_index]
        self.update_screens_slot()


class TitleWidget(QHBoxLayout):
    def __init__(self, text):
        super().__init__()
        title_label = QLabel()
        title_label.setText(text + ":")
        font = title_label.font()
        font.setBold(True)
        title_label.setFont(font)
        self.addWidget(title_label)


class ScreenWidget(QRadioButton):
    # TODO update the radio button when screen_id is changed outside (by command line for example)
    def __init__(self, screen_index, screen_selected_slot: callable):
        super().__init__()
        screen_size = QDesktopWidget().screenGeometry(screen_index).size()
        self.setText("Écran {}\n\nTaille:\n {}x{}". format(screen_index, screen_size.width(), screen_size.height()))
        self.pressed.connect(screen_selected_slot)


class GameParameterWidget(QFrame):
    def __init__(self, game_core: GameCore):
        super().__init__()
        self.setFrameStyle(FRAME_STYLE)
        self.setLineWidth(FRAME_LINE_WIDTH[0])
        self.setMidLineWidth(FRAME_LINE_WIDTH[1])

        nb_player_slider = ConfigTagedSlider("Nombre de joueurs",1, len(game_core.config.player_colors), game_core, "nbr_player")
        nb_life_slider = ConfigTagedSlider("Nombre de vies par joueur", 0, 10, game_core, "nbr_lifes")
        time_base_slider = ConfigTagedSlider("Temps de base", 0, 30, game_core, "pose_delay")
        time_reduction_slider = ConfigTagedSlider("Facteur de reduction du temps", 0, 1, game_core, "reduction_factor", True)
        time_start_slider = ConfigTagedSlider("Temps supplémentaire de la 1e pose", 0, 30, game_core, "start_pose_delay")

        main_layout = QVBoxLayout()
        main_layout.addWidget(nb_player_slider)
        main_layout.addWidget(nb_life_slider)
        main_layout.addWidget(time_base_slider)
        main_layout.addWidget(time_reduction_slider)
        main_layout.addWidget(time_start_slider)
        self.setLayout(main_layout)


class PoseParameterWidget(QFrame):
    def __init__(self, game_core: GameCore):
        super().__init__()
        self.setFrameStyle(FRAME_STYLE)
        self.setLineWidth(FRAME_LINE_WIDTH[0])
        self.setMidLineWidth(FRAME_LINE_WIDTH[1])

        alpha_color_btn = AlphaColorButton(game_core)
        intro_check = QCheckBox("Jouer l'animation d'intro")
        intro_check.setEnabled(False)  # TODO
        victory_check = QCheckBox("Jouer l'animation de victoire")
        victory_check.setEnabled(False)  # TODO
        player_size = ConfigTagedSlider2D("Taille silhouettes", 0, 2, game_core, "player_proportions", True)
        player_spacing = ConfigTagedSlider("Espacement silhouettes", 0, 1, game_core, "player_ratio_spacing", True)
        player_vertical_random = ConfigTagedSlider("Aléatoire vetical", 0, 0.2, game_core, "vertical_random_ratio", True)
        life_size = ConfigTagedSlider2D("Taille vies", 0, 2, game_core, "life_proportions", True)
        life_spacing = ConfigTagedSlider("Espacement vies", 0, 3, game_core, "multi_life_spacing_proportion", True)
        clock_dim = ConfigTagedSlider2D("Taille chronomètre", 0, 2, game_core, "timer_proportions", True)
        number_dim = ConfigTagedSlider2D("Taille digit chronomètre", 0, 1, game_core, "clock_dim_ratio", True)
        bodure_dim = ConfigTagedSlider("Taille bordure", 0, 1, game_core, "border_marge_proportion", True)

        main_layout = QVBoxLayout()
        self.setLayout(main_layout)
        main_layout.addWidget(alpha_color_btn)
        main_layout.addWidget(intro_check)
        main_layout.addWidget(victory_check)
        main_layout.addWidget(player_size)
        main_layout.addWidget(player_spacing)
        main_layout.addWidget(player_vertical_random)
        main_layout.addWidget(life_size)
        main_layout.addWidget(life_spacing)
        main_layout.addWidget(clock_dim)
        main_layout.addWidget(number_dim)
        main_layout.addWidget(bodure_dim)


class AlphaColorButton(QPushButton):
    # TODO same button for all player colors
    def __init__(self, game_core: GameCore):
        super().__init__("Changer la couleur du fond vert")
        game_core.size_config_changed_signal.connect(self.update_value_from_config)
        self.game_core = game_core
        self.pressed.connect(self.prompt_for_color)

    def prompt_for_color(self):
        html_color = QColorDialog.getColor().name(QColor.HexRgb)
        self.game_core.config.alpha_color = html_color
        self.game_core.size_config_changed_signal.emit(self.game_core.config)
        # TODO ask for changing intro and outro alpha color

    def update_value_from_config(self, config: TCConfig):
        self.setStyleSheet("background-color: "+config.alpha_color)


class ConfigFileWidget(QWidget):
    def __init__(self):
        super().__init__()

        save_btn = QPushButton("Save\nconfig")
        load_btn = QPushButton("Load\nconfig",)

        top_layout = QVBoxLayout()
        top_layout.addSpacing(200)
        main_layout = QVBoxLayout() #(QBoxLayout.BottomToTop)
        main_layout.addWidget(load_btn)
        main_layout.addWidget(save_btn)
        top_layout.addLayout(main_layout)
        self.setLayout(top_layout)


class TagedSlider(QWidget):
    def __init__(self, text, min: int, max: int, percent=False):
        super().__init__()
        self.percent = percent
        self.main_layout = QVBoxLayout()
        self.label = QLabel()
        self.text = text
        self.label.setText(text + ": None")
        self.main_layout.addWidget(self.label)
        self.slider = QSlider(Qt.Horizontal)
        if not percent:
            self.slider.setTickInterval(1)
            self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setMaximum(max * (100 if percent else 1))
        self.slider.setMinimum(min * (100 if percent else 1))
        self.main_layout.addWidget(self.slider)
        self.main_layout.setSpacing(1)
        self.setLayout(self.main_layout)

    def value(self):
        return self.slider.value() * (0.01 if self.percent else 1)

    def setValue(self, new_value):
        self.slider.setValue(new_value * (100 if self.percent else 1))
        self.label.setText(self.text + ": " + str(new_value))


class ConfigTagedSlider(TagedSlider):
    def __init__(self, name, min: int, max: int, game_core: GameCore, config_attibut_name: str, percent=False):
        super().__init__(name, min, max, percent)
        self.game_core = game_core
        self.config_attribut_name = config_attibut_name
        self.game_core.size_config_changed_signal.connect(self.update_value_from_config)
        self.slider.sliderReleased.connect(self.set_value_to_config)

    def update_value_from_config(self, config: TCConfig):
        self.setValue(config.__getattribute__(self.config_attribut_name) )

    def set_value_to_config(self):
        self.game_core.config.__setattr__(self.config_attribut_name, self.value())
        self.game_core.size_config_changed_signal.emit(self.game_core.config)


class ConfigTagedSlider2D(ConfigTagedSlider):
    def __init__(self, name, min: int, max: int, game_core: GameCore, config_attibut_name: str, percent=False):
        super().__init__(name, min, max, game_core, config_attibut_name, percent)
        self.slider2 = QSlider(Qt.Horizontal)
        if not percent:
            self.slider2.setTickInterval(1)
            self.slider2.setTickPosition(QSlider.TicksBelow)
        self.slider2.setMaximum(max * (100 if percent else 1))
        self.slider2.setMinimum(min * (100 if percent else 1))
        self.main_layout.addWidget(self.slider2)
        self.slider2.sliderReleased.connect(self.set_value_to_config)

    def value(self):
        return (self.slider.value() * (0.01 if self.percent else 1), self.slider2.value() * (0.01 if self.percent else 1))

    def setValue(self, new_value):
        self.slider.setValue(new_value[0] * (100 if self.percent else 1))
        self.slider2.setValue(new_value[1] * (100 if self.percent else 1))
        self.label.setText(self.text + ": " + str(new_value))


class SequenceWidget(QWidget):
    pass


class DriveWidget(QWidget):
    def __init__(self, game_core: GameCore):
        super().__init__()
        self.game_core = game_core

        self.game_state_label = QLabel()
        self.game_state_label.setText("Etat du jeu: INITIALISATION")
        game_core.game_state_changed.connect(self.set_game_state_slot)

        preview_button = QPushButton("\nPrévisualiser\n")
        preview_button.setStyleSheet("background-color: blue")
        preview_button.setEnabled(False) # TODO
        play_button = QPushButton("\nDémarrer\n")
        play_button.setStyleSheet("background-color: green")
        game_core.game_state_changed.connect(lambda gamestate: play_button.setEnabled(gamestate==GameState.WAIT_PLAY))
        abort_button = QPushButton("\nIntérrompre\n")
        abort_button.setStyleSheet("background-color: red")
        reload_button = QPushButton("\nRecharger\n")
        reload_button.setStyleSheet("background-color: yellow")
        game_core.game_state_changed.connect(lambda gamestate: reload_button.setEnabled(gamestate==GameState.WAIT_RELOAD))
        main_control = QHBoxLayout()
        main_control.addWidget(preview_button)
        main_control.addWidget(play_button)
        main_control.addWidget(abort_button)
        main_control.addWidget(reload_button)

        self.validation_buttons = []
        self.validations_layout = QHBoxLayout()

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.game_state_label)
        main_layout.addLayout(main_control)
        main_layout.addLayout(self.validations_layout)

        self.setLayout(main_layout)

        self.update_nb_player_slot()

        # --- Signaux ---
        # TODO preview
        play_button.pressed.connect(game_core.play_slot)
        abort_button.pressed.connect(game_core.end_game)
        reload_button.pressed.connect(game_core.reset_game_slot)
        game_core.size_config_changed_signal.connect(self.update_nb_player_slot) # TODO when reload or when config change ==> lock config when reloaded?

    def update_nb_player_slot(self):
        # TODO more detail about the player
        for w in self.validation_buttons:
            self.validations_layout.removeWidget(w)
            w.deleteLater()
        self.validation_buttons = [QPushButton("Valider\nJoueur "+str(k+1)) for k in range(self.game_core.config.nbr_player)]
        for k,w in enumerate(self.validation_buttons):
            self.validations_layout.addWidget(w)
            w.pressed.connect(functools.partial(self.game_core.validate_player_pose_slot, k))

    def set_game_state_slot(self, new_game_state: GameState):
        self.game_state_label.setText("Etat du jeu: "+str(new_game_state))
