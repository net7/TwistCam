
from enum import Enum
import os

from PyQt5.QtCore import QUrl, Qt, QSize, pyqtSignal
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent, QMediaResource, QMediaPlaylist
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import QLabel, QWidget, QStackedWidget, QLayout, QHBoxLayout


class intro_status(Enum):
    LOOPING = 0
    WAIT_INTRO = 1
    INTRO = 2
    IDLE = 3
    OUTRO = 4


class IntroWidget(QStackedWidget):
    """
    Used to manage introduction animation
    Provide 3 signals:
        - intro_end_signal : emitted when the intro ends, then the subwidget is displayed and status is IDLE
        - play_intro_signal : can be called to initiate intro when in LOOPING state
        - play_outro_signal : can be called to initiate outro when in IDLE state
    """
    # Signals
    intro_end_signal = pyqtSignal()

    def __init__(self, ressource_directory:str, subwidget:QWidget, alpha_color="#00FF00"):
        """
        :param ressource_directory: path to the ressource directory where video file can be found
        :param subwidget: the widget to display when the intro ends.
        :param alpha_color: the color for the alpha background
        """
        super().__init__()

        self.subwidgetIndex = super().addWidget(subwidget)
        self._locked = False
        self.status = intro_status.OUTRO
        ressource_directory = os.path.abspath(ressource_directory)
        self.anim_content = {
            "loop": QMediaContent(QUrl.fromLocalFile(os.path.join(ressource_directory,"introLoop.mp4"))),
            "intro":QMediaContent(QUrl.fromLocalFile(os.path.join(ressource_directory,"introIn_with_background.mp4"))),
            "outro":QMediaContent(QUrl.fromLocalFile(os.path.join(ressource_directory,"introOut_with_background.mp4")))
        }
        self._next_players = {}          # Used to pre load all possible animations
        self._next_video_widgets = {}
        self._active_players = []        # Used to remember all players a few second after giveup using it, in order to not seg fault.
        self._active_video_players = []

        # init preloading dictionnaries
        self._play_bugfix("loop", init=True)
        self._play_bugfix("intro", init=True)
        self._play_bugfix("outro", init=True)

        self._play_bugfix("outro")

        self.show()

    def _play_bugfix(self, media_name, init=False):
        """
        Play a media
        Fix SHLAG Qt's bug that display a black screen because first frames crash when a player is used twice
        """
        # Switch self to preloaded video_widget
        if not init:
            self.setCurrentIndex(self.indexOf(self._next_video_widgets[media_name]))
            self._active_video_players.append(self._next_video_widgets[media_name])
            self._active_players.append(self._next_players[media_name])
            self._active_players[-1].play()
            self._active_players[-1].stateChanged.connect(self._end_video_handler)

        # Preload a new Media
        self._next_video_widgets[media_name] = QVideoWidget()
        self._next_players[media_name] = QMediaPlayer()
        self._next_players[media_name].setVideoOutput(self._next_video_widgets[media_name])
        self._next_players[media_name].setMedia(self.anim_content[media_name])
        super().addWidget(self._next_video_widgets[media_name])

        # Delete old Media
        for w in self._active_players[:-4]:
            w.deleteLater()
        for w in self._active_video_players[:-4]:
            w.deleteLater()
        self._active_players = self._active_players[-4:]
        self._active_video_players = self._active_video_players[-4:]

    def _end_video_handler(self, status):
        """Called when the video end to decide what to do"""
        if self._locked:
            return
        self._locked =True
        if self.status == intro_status.INTRO:
            #print("end of intro")
            self.setCurrentIndex(1)
            self.status = intro_status.IDLE
            self.setCurrentIndex(self.subwidgetIndex)
            self.intro_end_signal.emit()
        elif self.status == intro_status.OUTRO:
            #print("end of outro")
            self._play_bugfix("loop")
            self.status = intro_status.LOOPING
        elif self.status == intro_status.LOOPING:
            #print("looping")
            self._play_bugfix("loop")
        elif self.status == intro_status.WAIT_INTRO:
            #print("Start intro")
            self._play_bugfix("intro")
            self.status = intro_status.INTRO
        else:
            raise Exception("Unkonw status change {}".format(self.status))
        self._locked = False

    def play_intro_slot(self):
        """Called by signal to initiate intro"""
        if self.status == intro_status.LOOPING:
            self.status = intro_status.WAIT_INTRO
        else:
            raise Exception("Can't play intro, current status is "+str(self.status))

    def play_outro_slot(self):
        """Called by signal to initiate outro"""
        if self.status == intro_status.IDLE:
            # TODO wait end of loop
            self._play_bugfix("outro")
            self.status = intro_status.OUTRO
        else:
            raise Exception("Can't play outro, current status is {}".format(self.status))

    def addWidget(self, qwidget):
        raise Exception("IntroWidget : this StackedWidget implementation doesn't allow more than one subwidget which " +
                        "must be given in the constructor")
