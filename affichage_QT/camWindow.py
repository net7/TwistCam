import functools
import os
import random
import sys
import threading

from PyQt5.QtCore import QSize, QObject, pyqtSignal, Qt, QUrl
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QLabel, QApplication, QDesktopWidget, QWidget, QFileDialog, QVBoxLayout, QBoxLayout, \
    QHBoxLayout, QStackedLayout, QStackedWidget, QSpacerItem

from Game_Core_2 import GameCore
from IntroWidget import IntroWidget
from LifeWidget import LifeWidget
from PoseWidget import PoseWidget
from TimerWidget import TimerWidget
from TwistCamConf import TCConfig


class CamWindow(QApplication):

    selected_screen_changed = pyqtSignal(int, list)
    full_screen_changed = pyqtSignal(bool)

    def __init__(self,
                 game_core: GameCore,
                 rage=False):

        self.game_core = game_core

        if game_core.config.verbose_level == 3:  # TODO utiliser logger
            print(" Initalisation CanWindow (en mode développeur)")
        # ----- Init de la fenêtre -----
        super().__init__([])
        game_core.init_pose_dict(game_core.config)
        self.setOverrideCursor(Qt.BlankCursor)

        if rage:
            rage_label = QLabel()
            if random.random() > 0.8:
                rage_label.setText("TVn7 allez vous faire voire")
            elif random.random() > 0.8:
                rage_label.setText("La v1 était déjà opérationnelle sans bug au JT4, peut etre moins belle mais ça passait")
            elif random.random() > 0.8:
                rage_label.setText("Là Tout marchait parfaitement des jours à l'avance, la V2 était nickel")
            elif random.random() > 0.8:
                rage_label.setText("On se marrait pendant les tests, le public a applaudi la démo.")
            elif random.random() > 0.8:
                rage_label.setText("Tous les modos étaitent trop chaud")
            else:
                rage_label.setText("TVn7 va niquer ta mère, une KC de 10s et 20s entre chaque...")
            rage_label.show()
            self.main_app.exec_()
            raise ValueError("C'est la faute a TVn7")

        # ----- Analyse des paramètres du constructeur -----
        # Ajouter le '#' dans les html color
        if type(game_core.config.alpha_color) == str and len(game_core.config.alpha_color) == 6:
            game_core.config.alpha_color = "#" + game_core.config.alpha_color

        # ======= Construction de l'interface =======
        # ------------------- TIMER -----------------
        self.timer = TimerWidget(game_core.config.ressources_directory, dev_mode=game_core.config.verbose_level == 3)
        self.push_right_layout = QBoxLayout(QBoxLayout.RightToLeft)
        self.push_right_layout.addWidget(self.timer)
        self.push_right_layout.setSpacing(0)
        self.timer_spacer = QSpacerItem(1, 1)
        self.push_right_layout.addSpacerItem(self.timer_spacer)
        # Signaux
        #game_core.size_config_changed_signal.connect(self.timer.size_config_changed_slot)   <== not called by signal but directly by cam_win function

        # ------------------- LIFEs -----------------
        self.lifes = []
        self.life_spacer = QSpacerItem(1, 1)
        self.multi_life_layout = QHBoxLayout()

        # ------------------- POSEs ----------------- (affiche les silhouettes)
        self.pose_widget = PoseWidget(game_core)
        self.main_layout = QVBoxLayout()
        self.pose_widget.setLayout(self.main_layout)
        self.main_layout.setSpacing(0)
        self.main_layout.addLayout(self.push_right_layout)
        self.main_layout.addSpacerItem(self.life_spacer)
        self.main_layout.addLayout(self.multi_life_layout)

        # ------------------- INTRO ----------------- (manage l'animation d'intro)
        self.intro = IntroWidget(game_core.config.ressources_directory, self.pose_widget)

        # -------------------  TOP  -----------------
        self.top_widget = QWidget()
        self.top_widget.setWindowTitle("TwistCam")
        self.top_widget.setWindowIcon(QIcon(os.path.join(game_core.config.ressources_directory, 'TCicon.png')))
        if game_core.config.verbose_level > 0:
            print("Déplacement du jeu sur l'écran cible")
        self.move_to_screen(game_core.config.screen_id, game_core.config.screen_order)
        self.selected_screen_changed.connect(self.move_to_screen)
        #self.top_widget.showFullScreen()
        self.top_widget.show()

        topLayout = QHBoxLayout()
        self.top_widget.setLayout(topLayout)
        topLayout.setContentsMargins(0,0,0,0)
        topLayout.addWidget(self.intro)

        # ========== Signaux ==========
        game_core.time_changed_signal.connect(self.timer.set_timer_value_slot)
        game_core.pose_changed_signal.connect(self.pose_widget.set_poses_slot)
        game_core.toggle_tick_signal.connect(self.pose_widget.toggle_color)

        game_core.life_lost_signal.connect(self.life_lost_slot)

        self.intro.intro_end_signal.connect(game_core.intro_end_slot)
        game_core.start_game_signal.connect(self.intro.play_intro_slot)
        game_core.end_game_signal.connect(self.intro.play_outro_slot)

        game_core.size_config_changed_signal.connect(self.size_config_changed_slot)
        game_core.reload_signal.connect(functools.partial(self.reload_slot, game_core))

        game_core.exit_game_signal.connect(self.exit_slot)
        self.exit = game_core.exit_slot

    @staticmethod
    def _get_component_sizes(config: TCConfig):
        screen_size = QDesktopWidget().screenGeometry(config.screen_id).size()
        border_size = screen_size.width() * config.border_marge_proportion
        timer_size = QSize(round((screen_size.width() - border_size * 2) * config.timer_proportions[0]),
                           round((screen_size.height() - border_size * 2) * config.timer_proportions[1]))
        player_size = QSize(round(screen_size.width() * config.player_proportions[0]),
                            round(screen_size.height() * config.player_proportions[1]))
        life_size = QSize(round((screen_size.width() - border_size * 2) * config.life_proportions[0]),
                          round((screen_size.height() - border_size * 2) * config.life_proportions[1]))
        return screen_size, border_size, timer_size, player_size, life_size

    def size_config_changed_slot(self, config: TCConfig):
        screen_size, border_size, timer_size, _, _ = self._get_component_sizes(config)
        # ------------- TIMER ---------------
        self.timer_spacer.changeSize(screen_size.width() - 2 * border_size - timer_size.width(), 1)
        self.timer.size_config_changed_slot(timer_size, config.clock_dim_ratio)

    def reload_slot(self, game_core: GameCore):
        # Get screen dimensions and compute UI element sizes
        screen_size, border_size, timer_size, _, life_size = self._get_component_sizes(game_core.config)

        # ------------- LIFEs ---------------
        life_size.setWidth(self.pose_widget.player_pixel_spacing * game_core.config.nbr_player)  # TODO WHAT?
        #   Remove old widgets
        for l in self.lifes: # TODO move dans LifeWidget?
            l.deleteLater()
        self.lifes = []
        for p in game_core.players:
            self.lifes.append(
                LifeWidget(game_core.config.ressources_directory,
                           QSize(self.pose_widget.player_pixel_spacing * (
                                       1 - game_core.config.multi_life_spacing_proportion),
                                 life_size.height()),
                           p.lifes,
                           dev_mode=game_core.config.verbose_level == 3)
            )
            #p.life_changed_signal.connect(self.lifes[-1].set_alive)
        #   Organize them
        life_offset = (screen_size.width() - 2 * border_size - life_size.width()) * 0.5
        self.multi_life_layout.setContentsMargins(life_offset, 0, life_offset, 0)
        self.multi_life_layout.setSpacing(self.pose_widget.player_pixel_spacing *
                                          game_core.config.multi_life_spacing_proportion)
        for life in self.lifes:
            self.multi_life_layout.addWidget(life)
        self.life_spacer.changeSize(1, screen_size.height() - 2 * border_size - timer_size.height() - life_size.height())

        # ----------- MAIN LAYOUT -----------
        self.main_layout.setContentsMargins(border_size, border_size, border_size, border_size)

    """
    def set_lifes_alive(self, nbs_alive):
        ""
        Set l'état vivant/mort des coeurs des différents LifeWidget.
        :param nbs_alive: int tuple, un tuple indiquant pour chaque LifeWidget le nombre de coeur mort.
        :return: None
        :raise: ValueError, si le nombre d'int fourni ne correspond pas au nombre de LifeWidget.
        ""
        if not len(nbs_alive) == len(self.lifes):
            raise ValueError("Nombre de valeures fournies (%d) différent du nombre de LifeWidget (%d)"
                             % (len(nbs_alive), len(self.lifes)))
        for i, life in enumerate(self.lifes):
            life.set_alive(nbs_alive[i])

    def set_poses_visibility(self, new_states):
        
        ""
        Set l'état visible/cache des différents LifeWidget et des silhouettes.
        :param new_states: boolean tuple
        :return: None
        :raise: ValueError, si le nombre de boolean fourni ne correspond pas au nombre de LifeWidget.
        ""
        if not len(new_states) == len(self.lifes):
            raise ValueError("Nombre de valeures fournies (%d) différent du nombre de LifeWidget (%d)"
                             % (len(new_states), len(self.lifes)))
        for i, life in enumerate(self.lifes):
            life.setVisibleKeepingSpace(new_states[i])
    """

    def exit_slot(self):
        # TODO balancer l'animation plutot que de couper
        print(" |Display jeu : commande force_exit")
        self.pose_widget.setWindowState(Qt.WindowMinimized)
        self.pose_widget.setWindowOpacity(0)
        super().exit()

    def toggle_fullscreen_slot(self):
        if self.top_widget.isFullScreen():
            self.top_widget.showNormal()
        else:
            self.top_widget.showFullScreen()
        self.full_screen_changed.emit(self.top_widget.isFullScreen())

    def move_to_screen(self, screen_id: int, screen_order: list):
        self.game_core.config.screen_id = screen_id
        self.game_core.config.screen_order = screen_order
        if self.game_core.config.verbose_level >= 2:
            print("Moving screen  screen order = {}, screen target {}".format(screen_order, screen_id))
        full_screen_state = self.top_widget.isFullScreen()
        if full_screen_state:
            self.top_widget.showNormal()
        self.top_widget.move(self._get_screen_position(screen_id, screen_order) + 1, 0)
        self.game_core.size_config_changed_signal.emit(self.game_core.config)
        if full_screen_state:
            self.top_widget.showFullScreen()

    def _get_screen_position(self, screen_id: int, screen_order: list):
        desk = QDesktopWidget()
        x = 0
        for k in screen_order:
            if screen_id == k:
                return x
            else:
                x += desk.screenGeometry(k).width()
        raise Exception("L'écran demandé n'est pas disponible")

    def life_lost_slot(self, player_id: int, remaining_lifes: int): # TODO move to LifeWidget
        self.lifes[player_id].set_alive(remaining_lifes)
        self.pose_widget.start_player_blink_slot(player_id, life_lost=True)
"""
def SelectScreen(screen_id):
    if screen_id < 0 or screen_id >= QDesktopWidget().screenCount():
        print("No pre-selected screen")
        if QDesktopWidget().screenCount() == 1 :
            print("Only one screen, skip selection.")
            screen_id = 0
        else:
            screen_id = AskForScreenSelection()


    screen_size = QDesktopWidget().screenGeometry(screen_id).size()
    print("Screen size : ({},{})".format(screen_size.width(), screen_size.height()))
    return screen_id, screen_size

def AskForScreenSelection():
    screen_id = None
    while screen_id is None:
        print("Available screens :")
        for k in range(QDesktopWidget().screenCount()):
            size = QDesktopWidget().screenGeometry(k).size()
            print(" - screen {} : dimension {}x{}".format(k, size.width(), size.height()))
        screen_id = int(input("Which screen to use ?"))
    return screen_id
"""

if __name__ == "__main__":
    print("TwistCamWindow test start...")
    ressources = "../ressources/"
    ALPHA_COLOR = "FF00FF"
    for k in range(1, 4):
        tc_win = CamWindow(ressources, -1, k,
                           timer_proportions=((0.14, 0.25) if k == 1 else (0.21, 0.40)),
                           life_proportions=((0.12, 0.75) if k == 1 else (1., 0.15)),
                           alpha_color=ALPHA_COLOR,
                           dev_mode=False)
        tc_win.pose_widget.set_poses_slot(list(tc_win.pose_widget.pose_dict.keys())[2:k + 2])
        tc_win.top_widget.show()
        tc_win.main_app.exec_()
        del tc_win.main_app
    print("TwistCamWindow test finished.")
    sys.exit()
