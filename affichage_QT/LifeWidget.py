import math

import os
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel, QGridLayout, QApplication


class LifeWidget(QLabel):
    DEFAULT_FUCKING_MARGIN = 9
    LIFE_PIXMAP = None
    DEAD_PIXMAP = None

    def __init__(self, ressource_directory, dim: QSize, nbr_life, dev_mode=False):
        """
        Generate a Widget displaying lifes.
        :param ressource_directory: String, the path to ressource directory
        :param dim: QtCore.QSize, the dimension of the space allocated to LifeWidget.
        :param nbr_life: int, the number of heart to display.
        :param dev_mode: boolean [Facultative (default False)], if True debug informations are displayed.
        """
        super().__init__()
        if LifeWidget.LIFE_PIXMAP is None:
            LifeWidget._load_ressources(ressource_directory)

        self.setMaximumSize(dim)
        self.setMinimumSize(dim)
        # Setup grid layout
        self.main_grid_layout = QGridLayout(self)
        ratio_h_w = (dim.height() * LifeWidget.LIFE_PIXMAP.width()) / (dim.width() * LifeWidget.LIFE_PIXMAP.height())
        self.nbr_columns = math.ceil((nbr_life / ratio_h_w) ** 0.5)
        self.nbr_lines = math.ceil(nbr_life / self.nbr_columns)
        self.column_size = dim.width() / self.nbr_columns
        self.line_size = dim.height() / self.nbr_lines
        self.main_grid_layout.setSpacing(0)
        # Resize pictures
        self.life_pix = LifeWidget.LIFE_PIXMAP.scaled(self.line_size,
                                                     self.column_size, Qt.KeepAspectRatio)  # TODO resize globaly
        self.dead_pix = LifeWidget.DEAD_PIXMAP.scaled(self.line_size,
                                                     self.column_size, Qt.KeepAspectRatio)  # TODO resize globaly
        # Setup heart images
        self.life_labels = []
        for k in range(nbr_life):
            self.life_labels.append(QLabel())
            self.main_grid_layout.addWidget(self.life_labels[-1], k // self.nbr_columns, k % self.nbr_columns)
            if dev_mode:
                self.life_labels[-1].setStyleSheet("background-color:pink")
        if dev_mode:
            self.setStyleSheet("background-color:purple")
            print("LifeWidget Dimensions:\n | screen_size : %s, %s\n | ratio : %s \n | lines : %s\n | rows : %s" % (
                dim.height(), dim.width(), ratio_h_w, self.nbr_lines, self.nbr_columns))
        # Adjust border for centering
        horizontal_space = dim.width() - self.life_pix.width() * self.nbr_columns
        vertical_space = dim.height() - self.life_pix.height() * self.nbr_lines
        self.setContentsMargins(math.floor(horizontal_space * 0.5 - self.DEFAULT_FUCKING_MARGIN),
                                math.ceil(vertical_space*0.5 - self.DEFAULT_FUCKING_MARGIN),
                                math.ceil(horizontal_space*0.5 - self.DEFAULT_FUCKING_MARGIN),
                                math.floor(vertical_space*0.5 - self.DEFAULT_FUCKING_MARGIN))
        self.set_alive(nbr_life)

    def set_alive(self, nbr_alive):
        """
        Set labels 'dead' or 'alive'.
        :param nbr_alive: int, number of labels to set as 'dead'.
        :return: None
        """
        #TODO tout passer en positif!
        for i, label in enumerate(self.life_labels):
            label.setPixmap(self.life_pix if i < nbr_alive else self.dead_pix)
        if nbr_alive == 0:
            self.setVisibleKeepingSpace(False)

    def setVisibleKeepingSpace(self, new_state: bool):
        """
        Show/Hide all life labels but unlike setVisible, the widget keep allocating space.
        :param new_state: boolean, the new state of visibility.
        :return: None
        """
        # TODO invert boolean meaning to correspond to validity
        for label in self.life_labels:
            label.setVisible(new_state)

    @classmethod
    def _load_ressources(cls, ressource_directory:str):
        cls.LIFE_PIXMAP = QPixmap(os.path.join(ressource_directory, "life.png"))
        cls.DEAD_PIXMAP = QPixmap(os.path.join(ressource_directory, "dead.png"))


if __name__ == "__main__":
    print("LifeWidget test start...")
    import sys
    import time
    for size in [(300, 200), (100,200)]:
        for k in range(1, 15):
            main_app = QApplication(sys.argv)
            ressources = "../ressources/"
            main_window = LifeWidget(ressources,
                                     QSize(size[0], size[1]),
                                     k, dev_mode=True)
            main_window.setWindowTitle("Test LifeWidget")
            main_window.set_alive(1)
            main_window.show()
            main_app.exec_()
            del(main_app)
    print("LifeWidget test finished.")
    sys.exit()