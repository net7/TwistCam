import os
import random
import threading
import time

from PyQt5.QtCore import QSize, Qt, QRect
from PyQt5.QtGui import QPainter, QPixmap, QColor
from PyQt5.QtWidgets import QLabel, QApplication, QDesktopWidget

import pose
from Game_Core_2 import GameCore
from TwistCamConf import TCConfig


class PoseWidget(QLabel):
    def __init__(self, game_core: GameCore):  # TODO independant de game_core?

        # Setup widget
        super().__init__()
        if game_core.config.verbose_level == 3:
            self.setStyleSheet("background-color:blue")
        else:
            self.setAttribute(Qt.WA_TranslucentBackground, True)

        self.config = game_core.config

        self.toggle_state = 0
        self.toggle_colors = []
        self.toggle_cache_pixmap = []

        # Setup poses
        self.current_silhouette = ()
        self.player_rect = QRect()
        self.player_pixel_spacing = 0
        self.pose_dict = game_core.POSE_DICT
        self.vertical_random = ()
        self.order = ()

        # Init constant for pose drawing
        self.pose_hauteur = None
        self.pose_largeur = None
        self.player_pixel_spacing = None

        game_core.size_config_changed_signal.connect(self.size_changed_slot) # TODO move to cam_win
        game_core.reload_signal.connect(self.reload_slot)
        game_core.pose_changed_signal.connect(self.set_poses_slot)
        game_core.player_validation_signal.connect(self.start_player_blink_slot)

    def size_changed_slot(self, config: TCConfig):
        if (config.locked):  # TODO bugfix, Workaround SLAG. When locking the config,pose_widget repaint a black screen
            return
        screen_size = QDesktopWidget().screenGeometry(config.screen_id).size()
        player_size = QSize(round(screen_size.width() * config.player_proportions[0]),
                            round(screen_size.height() * config.player_proportions[1]))

        self.setMaximumSize(screen_size)
        self.setMinimumSize(screen_size)
        self.toggle_cache_pixmap = list([QPixmap(screen_size), QPixmap(screen_size)])
        self.player_rect = QRect((screen_size.width() - player_size.width()) * 0.5,
                                 (screen_size.height() - player_size.height()) * 0.5,
                                 player_size.width(),
                                 player_size.height())

        random_sil_pixmap = pose.get_random_nonnull_silhouette(self.pose_dict).pixmap
        vertical_scale = self.player_rect.height() / random_sil_pixmap.height()
        self.pose_hauteur = round(random_sil_pixmap.height() * vertical_scale)
        self.pose_largeur = round(random_sil_pixmap.width() * vertical_scale)
        self.player_pixel_spacing = config.player_ratio_spacing * self.pose_largeur

    def reload_slot(self):
        self.vertical_random = tuple(
            self.config.vertical_random_ratio * self.pose_hauteur * random.random() for k in self.config.player_colors[:self.config.nbr_player])

    def toggle_color(self):
        self.toggle_state = (self.toggle_state + 1) % 2
        self.setPixmap(self.toggle_cache_pixmap[self.toggle_state])

    def set_poses_slot(self, pose_names):
        """Set la pose affichée à l'écran en la designant par son nom"""
        self.current_silhouette = ()
        for pose_name in pose_names:
            if pose_name is None or pose_name == "":
                self.current_silhouette = self.current_silhouette + (None,)
            elif pose_name in self.pose_dict.keys():
                self.current_silhouette = self.current_silhouette + \
                                          (self.pose_dict[pose_name].get_silhouette(),)
            else:
                print("Warning: Set_Pose d'une pose inconnue :", pose_name)
        #self.toggle_colors = [list(self.config.player_colors), list(self.config.player_colors)]
        n = len(self.current_silhouette)
        self.order = random.sample(range(n), n)
        self.toggle_colors = list([[QColor(c) for c in self.config.player_colors[:len(pose_names)]],
                                   [QColor(c) for c in self.config.player_colors[:len(pose_names)]]])
        self.repaint_poses(0)
        self.repaint_poses(1)
        self.toggle_color()

    def repaint_poses(self, toggle_index):
        painter = QPainter(self.toggle_cache_pixmap[toggle_index])
        #painter.eraseRect(self.rect())
        painter.fillRect(self.rect(), QColor(self.config.alpha_color))

        if self.current_silhouette is not None:

            left_center = self.player_rect.left() + 0.5 * self.player_rect.width() - self.player_pixel_spacing * (
                len(self.current_silhouette) - 1) * 0.5

            for k in self.order:
                if self.current_silhouette[k] is not None:
                    new_pixmap = QPixmap(self.pose_largeur, self.pose_hauteur)
                    new_pixmap.fill(self.toggle_colors[toggle_index][k])
                    new_pixmap.setMask(self.current_silhouette[k].pixmap.scaled(self.pose_largeur, self.pose_hauteur).mask())
                    painter.drawPixmap(int(left_center + k * self.player_pixel_spacing - (self.pose_largeur * 0.5)),
                                       self.player_rect.top() + self.vertical_random[k],
                                       self.pose_largeur,
                                       self.pose_hauteur,
                                       new_pixmap)
        painter.end()

    def start_player_blink_slot(self, player_id, life_lost=False): # TODO connect
        """
        Start a thread that make de color of the silhouette of the given player blinking
        :param player_id: the player to blink
        :param life_lost: blink color is white if False, red if True
        """
        white_blinking_thread = threading.Thread(target=self._color_blink,
                                                 args=(player_id, "#D00000" if life_lost else"#FFFFFF", self.config.animation_delay))
        white_blinking_thread.start()

    def _color_blink(self, player_id, new_HTML_color, duration):
        new_color = QColor(new_HTML_color)
        self.toggle_colors[0][player_id] = new_color
        self.repaint_poses(0)
        if duration > 0:
            time.sleep(duration)
        self.toggle_colors[1][player_id] = new_color
        self.repaint_poses(1)


if __name__ == "__main__":
    # TODO
    print("PoseWidget test start...")
    import sys
    for size in [(1200, 600), (100, 200), (600, 100)]:
        for k in range(2, 6):
            main_app = QApplication(sys.argv)
            ressources = "../ressources/"
            main_window = PoseWidget(ressources,
                                     QSize(size[0], size[1]),
                                     QSize(size[0], size[1]/(k*0.5)),
                                     dev_mode=False)
            print("Test des poses :", list(main_window.pose_dict.keys())[1:k])
            main_window.set_poses_slot(list(main_window.pose_dict.keys())[1:k])
            main_window.setWindowTitle("Test PoseWidget")
            main_window.show()
            main_app.exec_()
            del main_app
    print("PoseWidget test finished.")
    sys.exit()
