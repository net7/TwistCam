from PyQt5.QtWidgets import QWidget, QHBoxLayout, QTabWidget


class KeyEventTabWidget(QTabWidget):

    def __init__(self, key_event_callback: callable):
        super().__init__()
        self.key_event_callback = key_event_callback

    def keyPressEvent(self, QKeyEvent):
        self.key_event_callback(QKeyEvent)