TwistCam
=====================

### Setup:

- install required python3 modules: <br>
  - `PyQt5` for main game application. <br>
  - `matplotlib` and `numpy` if you want to use silhouette_generator.  
  - `twisted` for remote control.
    - For Windows PC, require an extension : `pip install pypiwin32`
  - `netifaces` for displaying host ip automaticaly
- For window a codec is needed : you can install QLightCodecBasic 
> I recommend to make a python virtual env. `python3 -m venv twicamEnv` <br>
> then activate it with `source twistcamEnv/bin/activate`

- Generate some silhouettes for the game (See __Silouette generator__
chapter).

> TVn7 require `1080x720 i50` video format. In order to set your output
you can use `xrandr` (hard, and I can't give you the exact command) 
or `lxrandr` which is a graphical interface for setuping this two 
parameters and placing your screen on the right.

> You can also use arandr  to make sure your output is virtually located 
on the right of your main screen but notice that arandr reset lxrandr 
configurations and vice versa.

> If you use multiple desktop, it's a good idea to set the game window 
to be displayed on all desktops to avoid miss manipulations.

### Usage, Game:

- Set config file : `TwistCamConf.py`, default configuration is for 3 players,
another configuration file is committed for 1 player (TwistCamConf(1player).py).

> If the game does not scale well on screen, first try restart the game then  
if it doesn't solve the problem it may be 'screen_id' value which 
define the index of the screen to use, which may change from a computer to another.
If it doesn't solve then try changing '*_proportions' configs.

- Into your shell run `python3 Main_TwisterCam_2.py` 
(you can eventually add -v or -vv or -vvv as
parameter to get debug information).

#### Controling the game from Control Room:
Use __Réglage__ tab to adjsut config, mainly you should move the game
window to the desired screen. <br>
Use __Contrôle__ tab drive the game
you must reload the game then start it when TVn7 displays it on screen.
You can also validate any player at any time with corresponding button.

You can also use key bindings to start the game by pressing __p__ key or
validate up to 4 players by pressing respectively __a__, __space__,
__enter__, or __+__ keys (these key shortcut can be changed in the
config)

Press __Esc__ key to kill the game.

#### Controling the gale remotely
Moderator can connect to te game using `nc game_id game_port` where
__game_id__ is th IP adress of the pc running the game, (this IP is
displayed by the console when the game starts) and __game_port__ is the
port defined in the config (by default 1117) (port is also displayed in
the game console).

### Usage, Silhouette generator:

To generate manually a silhouette:

- open silhouette_generator/main_manuel.py file
- set angle values as you wish, by editing tthe first line of the script
- run `python3 silhouette_generator/main_manuel.py`
- follow printed instruction to validate the preview.

To generate automatically some silhouettes:

- run `python3 silhouette_generator/main_random.py`
- follow printed instruction to validate which generated images 
should be kept.

All output images are stored in 'silhouette_generator/generation_random/'

To use then in the main program copy them into 'ressources/silhouette_storage/ ' 
directory, then run the main program once to generate associated 
config files in 'ressources/pose_configs/ '. 

You can eventually customise these configuration files. 

### TODO

- [X] Implement graphical interface V2.

- [X] Add multi-player mode.

- [X] Add color feedback animation for player.

- [X] Add Intro and outro video clip.
    - [X] Outo animated manuelly.
    - [X] Intro/Outro video pre-computed.
- [X] Patch end of game bugs.

- [X] Patch graphical bugs. (not critical)

- [X] Patch validation/reload bugs.

- [X] Add remote control (nc or what ever) instead of key hitting in 
game mode.

- [ ] Remove custom configurations system and custom shell system.

- [X] Add a default set of silhouettes.

- [ ] Add not random pose selection and on the fly pose selection.

- [ ] Add interactive poses.

- [ ] Use Locks (Not thread safe).
