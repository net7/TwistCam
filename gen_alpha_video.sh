!#/bin/bash
# usage : 'gen_alpha_video.sh [alpha_color]'

# Donne une valeur par défaut au paramètre
ALPHACOLOR="0x00FF00"
echo "param $#"
if [[ "$#" -gt 0 ]];
then 
	ALPHACOLOR="$1";
fi;

# Remplace le fond noir de l'intro d'origine par un fond de la couleur indiqué en paramètre.
ffmpeg -y -i ressources/introIn.mp4 -framerate 25 -video_size 1920x1080 -filter_complex "[0]split[a][b]; [b]drawbox=color=$ALPHACOLOR:thickness=600[bg];[a]colorkey=color=0x000000:similarity=0.2[alpha];[bg][alpha]overlay[v]" -map "[v]" -c:v libx264 -r 25 ressources/introIn_with_background.mp4

# Reverse the video for the outro
ffmpeg -y -i ressources/introIn_with_background.mp4 -vf reverse ressources/introOut_with_background.mp4

