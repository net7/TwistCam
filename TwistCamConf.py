class TCConfig:
    def __init__(self):
        # ===========================================================================================================
        # ========================================= GAME CORE PARAMETER =============================================

        # Modes possibles : random  (pour une génération automatique)
        self.generator_mode = "random"

        # Conf de la couleur du fond vert utilisé par TVn7 (HTML color)
        self.alpha_color = "00FF00"

        # Nombre de vie par joueur.
        self.nbr_lifes = 2

        # Nombre de pose par séquence.
        self.nbr_poses = 12

        # Nombre de joueur dans la partie.
        self.nbr_player = 3

        # Nom de la 1e pose.
        # Si défini, la première pose d'une séquence sera toujour celle-ci
        self.start_pose = ""

        # Délais  de base pour chaque pose
        self.pose_delay = 10
        self.reduction_factor = 0.75
        # Délais spécial pour la 1e pose.
        self.start_pose_delay = 10

        # delai des animation de reussite/echec
        self.animation_delay = 0.7

        # inital visibility (Not used yet)
        self.initial_visibility = None

        # Séquence executé quand un joueur gagne la partie, (intervale entre chaque image, liste des images)
        self.wi_seq = (0.3, ["Strong8080", "standup8888"]*3 + ["holla25310","holla18516", "holla25310", "holla31916", "holla46043", "holla52584", "holla46043", "holla31916"]*2)

        # Mode de fonctionnement du timer (FULL_GAME | EACH_POSE | DISABLED)
        # FULL_GAME : means 1 timer for the full sequence.
        # EACH_POSE : means the timer is reset with each pose.
        # DISABLED  : no timer.
        from Game_Core_2 import TimerMode
        self.timer_mode = TimerMode.EACH_POSE

        # Touche du clavier a utiliser pour valider les différents joueurs
        self.validate_keys = (65, 32, 16777220, 43) # keys : 'a', 'space', 'enter', '+'

        self.player_colors = ("#8904B1", "#FF8000", "#01DFD7", "#0404B4", "#FF0000")

        # Enable remote control with tcp connexions
        self.enable_remote_control = True

        self.verbose_level = 1
        # 0:    quiet
        # 1:    basic info
        # 2:    silhouette loading debug info,
        # 3:    graphical debug info

        # duration between two core updates
        self.timer_tick_duration = 0.1

        # Init constant for toggling
        self.toggle_duration = 2 * self.animation_delay

        self.max_difficulty_level = None  # Do not edit


        # ===========================================================================================================
        # =========================================== CAM WINDOW PARAMETER ============================================

        self.ressources_directory = "./ressources"

        # L'identifiant de l'écran a utiliser, a priori -1, testez différentes valeur si le dimensionnement de l'interface est raté.
        self.screen_id = 0
        self.screen_order = [0]

        # Ecart minimal entre l'interface et le bord de l'écran
        # Valeurs dans [0 .. 1] (1 équivaut a la largeur de l'écran)
        self.border_marge_proportion = 0.03
        # Taille (horizontal, vertical) du chronomètre proportionnellement a la taille de l'écran
        # Valeurs dans [0 .. 1] (1 équivaut a la largeur de l'écran)
        self.timer_proportions = (0.14, 0.25)
        # Tailles (horizontal, vertical) des chiffres du chronomètre proportionnellement a la taille totale.
        # Valeurs dans [0 .. 1] (1 équivaut a la largeur du chronomètre)
        self.clock_dim_ratio = (0.9, 0.5)
        # Taille des barre de vie de chaque joueur
        # Valeurs dans [0 .. 1] (1 équivaut a la largeur de l'écran)(RQ la donnée horizontal est ingorée en multijoueur)
        self.life_proportions = (0.5, 0.15)
        # Taille des silhouette de chauqe joueur
        # Valeurs dans [0 .. 1] (1 équivaut a la largeur de l'écran)
        self.player_proportions = (1, 0.7)
        # Écart entre chaque joueur
        # /!\ dans [0 .. 1] (1 équivaut à la largeur D'UN JOUEUR !!)
        self.player_ratio_spacing = 0.25
        self.vertical_random_ratio = 0.1
        # Écart entre chaque barre de vie en mode multiplayer
        # /!\ dans [0 .. 1] (1 équivaut à la largeur D'UN JOUEUR !!)
        self.multi_life_spacing_proportion = 0.10

        # ======= Internal data =========
        self.locked = False  # DO NOT EDIT
        self.edit_signal = None  # DO NOT EDIT

    def config_lock(self):
        if self.verbose_level > 1 :
            print(' |Config lock')
        self.locked = True
        if self.edit_signal is not None:
            self.edit_signal.emit(self)

    def config_unlock(self):
        if self.verbose_level > 1 :
            print(' |Config unlock')
        self.locked = False
        if self.edit_signal is not None:
            self.edit_signal.emit(self)

    def change_value(self, value_name, new_value):  # TODO not used
        if self.locked:
            print("Warning : config edition is locked")
        else:
            self.__setattr__(value_name, new_value)
            if self.edit_signal is not None:
                self.edit_signal.emit(self)

    def copy(self):
            # DO NOT EDIT
            new_conf = TCConfig()
            new_conf.__dict__ = self.__dict__.copy()
            return new_conf
